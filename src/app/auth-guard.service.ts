import {
  Injectable
} from '@angular/core';
import {
  CanActivate,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  UrlTree
} from '@angular/router';
import {
  LoginService
} from './login.service';
import {
  Observable, Subject
} from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Login } from './login';

@Injectable()
export class AuthGuard implements CanActivate {
  permissions: any;
  // accessable: Subject<boolean> = new Subject();


  constructor(private loginS: LoginService, private http: HttpClient) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable < boolean | UrlTree > | Promise < boolean | UrlTree > {
    // this.http.get('assets/login.json').subscribe((response: Login) => {
    //   this.accessable.next(response.admin);
    // });
    // return this.accessable;
    // let requiredLoginState = route.data.requiredLoginState;
    return this.loginS.checkIfUserIsLoginInTheServer();
    // return this.loginS.loggedIn;
  }

}
