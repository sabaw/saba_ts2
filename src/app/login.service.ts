import {
  Injectable
} from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';
import { BehaviorSubject, Subject, Observable } from 'rxjs';
import { Login } from './login';

@Injectable()
export class LoginService {
  $loggedIn: Subject<boolean> = new Subject();
  constructor(private http: HttpClient) {}

  checkIfUserIsLoginInTheServer(): Observable<boolean> {
    this.http.get('assets/login.json').subscribe((res: Login) => {
      this.$loggedIn.next(res['admin'])
    });
    return this.$loggedIn;
  }

}
