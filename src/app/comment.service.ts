import {
  Injectable
} from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';
import {
  Comment
} from './comments/singlleComment..interface';
import {
  Subject, BehaviorSubject
} from 'rxjs';

@Injectable()
export class CommentService {
  data: Subject < Comment[] > = new Subject;
  dataCache: Comment[];
  lastId: number;
  showModal: BehaviorSubject < boolean > = new BehaviorSubject(false);
  
  constructor(private http: HttpClient) {
    this.data.subscribe((data: Comment[]) => {
      this.dataCache = data;
    });
  }

  getJSON() {
    return this.http.get('assets/comments.json').subscribe((data: Comment[]) => {
      this.data.next(data);
    });
  }

  pushNewComment(commentData: Comment) {
    this.data.next([
      commentData,
      ...this.dataCache
    ]);
  }
}
