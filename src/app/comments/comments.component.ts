import {
  Component,
  OnInit,
  OnDestroy
} from '@angular/core';
import {
  CommentService
} from '../comment.service';
import {
  Comment
} from './singlleComment..interface';

import Swal from 'sweetalert2'
import {
  NgbModal,
  NgbActiveModal
} from '@ng-bootstrap/ng-bootstrap';
import {
  Subject,
  BehaviorSubject,
  Subscription,
  Observable
} from 'rxjs';
import {
  log
} from 'util';
import {
  CommentFormComponent
} from './comment-form/comment-form.component';
import {
  THIS_EXPR
} from '@angular/compiler/src/output/output_ast';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css'],
  providers: [NgbActiveModal, NgbModal]
})
export class CommentsComponent implements OnInit, OnDestroy {

  commentList: Comment[];
  subscription: Subscription;
  showModalSubscription: Subscription;


  constructor(
    private commentS: CommentService,
    private modalService: NgbModal,
    private activeModal: NgbActiveModal,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.commentS.getJSON();
    this.subscription = this.commentS.data.subscribe((data: Comment[]) => {
      this.commentList = data;
    })
    // this.router.navigate(['/profile'], {relativeTo: this.route})
  }

  onAddNewComment(content) {
    this.commentS.showModal.next(true);
      var modal = this.modalService.open(CommentFormComponent, {
        size: 'lg',
        centered: true
      });
      modal.componentInstance.commentListLength = this.commentList.length
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    // this.showModalSubscription.unsubscribe();
  }
}
