import {
  Component,
  OnInit,
  Input,
  OnDestroy
} from '@angular/core';
import {
  CommentService
} from 'src/app/comment.service';
import {
  Comment
} from '../singlleComment..interface';
import {
  NgbActiveModal
} from '@ng-bootstrap/ng-bootstrap';
import {
  Subscription
} from 'rxjs';
@Component({
  selector: 'app-comment-form',
  templateUrl: './comment-form.component.html',
  styleUrls: ['./comment-form.component.css']
})
export class CommentFormComponent implements OnInit, OnDestroy {

  comment: Comment = {
    id: -1,
    name: '',
    email: '',
    comment: ''
  };
  subscription: Subscription;
  @Input() commentListLength;
  constructor(private commentS: CommentService, private activeModal: NgbActiveModal) {}

  ngOnInit() {
    this.subscription = this.commentS.showModal.subscribe((value) => {
      console.log("form subscription", value);
    })
  }

  OnSendCommentClick() {
    console.log(this.comment);
    this.commentS.showModal.next(false);
    this.comment.id = this.commentListLength
    this.commentS.pushNewComment(this.comment)
    this.activeModal.close();

    this.comment = {
      id: -1,
      name: '',
      email: '',
      comment: ''
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
